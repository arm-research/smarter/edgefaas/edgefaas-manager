#!/bin/sh
# Copyright (c) 2020, Arm Ltd
# Author: Luis E. P.
#
# This script is used to manage EdgeFaaS

# need to add host option
FAAS=edgefaas
PORT=8080
HOST=$FAAS:$PORT

# unset variables
unset ACTION URL FUNCTION

echo "arguments:"
echo $@

if [ $# -eq 0 ]
then
  echo "Arguments needed. Run manager.sh -h for help"
  exit 1
fi

while getopts ":li:d:H:h" opt; do
  case ${opt} in
    l )
      if [ -n "$ACTION" ]
      then
          echo "Please provide only one action (l/i/d)"
          exit 1
      fi
      ACTION=list
      ;;
    i )
    if [ -n "$ACTION" ]
      then
          echo "Please provide only one action (l/i/d)"
          exit 1
      fi
      ACTION=install
      URL=$OPTARG
      ;;
    d )
      if [ -n "$ACTION" ]
      then
          echo "Please provide only one action (l/i/d)"
          exit 1
      fi
      ACTION=delete
      FUNCTION=$OPTARG
      ;;
    H )
      HOST=$OPTARG
      ;;
    h )
      echo "Usage:"
      echo "  manager.sh -l [-H <edgefaas>:<port>]                  : lists installed functions"
      echo "  manager.sh -i <function url> [-H <edgefaas>:<port>]   : installs function located at <function url>"
      echo "  manager.sh -d <function name> [-H <edgefaas>:<port>]  : deletes function named <function name>"
      exit 0
      ;;
    : )
      echo "-$OPTARG requires an argument"
      exit 1
      ;;
  esac
done

if [ -z "$ACTION" ]
then
  echo "No action provided. Run manager.sh -h for help"
  exit 1
fi

case ${ACTION} in
  list )
    echo "listing functions installed at ${HOST}"
    curl -s ${HOST}/mgmt
    ;;
  install )
    echo "installing ${URL} to ${HOST}"
    curl -s ${HOST}/mgmt -d "${URL}"
    ;;
  delete )
    echo "deleting ${FUNCTION} from ${HOST}"
    curl -s -X "DELETE" ${HOST}/mgmt -d "${FUNCTION}"
    ;;
esac
exit
