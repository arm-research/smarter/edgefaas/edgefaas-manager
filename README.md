# EdgeFaaS Manager

The `manager.sh` script provides a wrapper for the basic list/install/delete management operations for EdgeFaaS.

The `manager.sh` script can be leveraged by the edgefaas-manager container to perform management operations via the Kubernetes interface"

For now, the EdgeFaaS Manager containers are deployed as Jobs.
There is a Job file for each of the actions in the `yaml` directory.

Go into each `yaml` file and set the variables under `env` to the correct values.

Retrieve the EdgeFaaS pod IP address:
```
$ kubectl describe pod/edgefaas | grep IP
IP:           10.42.0.19
IPs:
  IP:  10.42.0.19
$ FAAS=10.42.0.19
```

For the install file, set the URL variable.
For the delete file, set the FUNCTION variable.

To run, apply the Job file:
```
$ kubectl apply -f yaml/list.yaml
job.batch/edgefaas-list created
```

To see the outcome of the Job, find the "youngest" pod that ran the job, then print its logs:
```
$ kubectl get pods --selector=job-name=edgefaas-list
NAME                  READY   STATUS      RESTARTS   AGE
edgefaas-list-xdrt9   0/1     Completed   0          41s
$ kubectl logs edgefaas-list-xdrt9
arguments:
-l -H10.42.0.3:8080
listing functions installed at 10.42.0.3:8080
echo
```

Here, we see that only the `echo` function has been installed.

One of the drawbacks of using Jobs is that a job needs to be deleted before being ran again:
```
$ kubectl delete jobs/edgefaas-list
job.batch "edgefaas-list" deleted
```
